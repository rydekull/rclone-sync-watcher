#!/bin/bash

echo "rclone sync watcher started at: $(date '+%Y%m%d %H:%M:%S')"
while true 
do
  rclone sync gdrive: ${HOME}/rclone/gdrive 2>&1 | grep -v " - ignoring"
  inotifywait --recursive --timeout 30 -e modify,delete,create,move ${HOME}/rclone/gdrive > /dev/null 2>&1 
  if [ "$?" = "0" ]
  then
    rclone sync ${HOME}/rclone/gdrive gdrive: 2>&1 | grep -v " - ignoring"
  fi
done
