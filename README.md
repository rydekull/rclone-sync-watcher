Basic setup of rclone to google drive

```
./ansible.yml
ln -s $(pwd) ~/rclone # Assuming we will use ~/rclone for everything
rclone config # Go through all the hoops, make sure you set up Google drive and call it "gdrive"
```

If you want it to run in the background as your machine boot, set it up with systemd
```
mkdir -p ~/.config/systemd/user
cp rclone.service ~/.config/systemd/user/rclone.service 
systemctl --user enable rclone
systemctl --user start rclone
```

You can see that it is running with the help of:
`journalctl --user -f`
